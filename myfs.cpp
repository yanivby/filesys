#include "myfs.h"
#include <string.h>
#include <iostream>
#include <math.h>
#include <sstream>
#include <vector>
#include <algorithm>
const char *MyFs::MYFS_MAGIC = "MYFS";

void print_bytes(const void *object, size_t size);

MyFs::MyFs(BlockDeviceSimulator *blkdevsim_):blkdevsim(blkdevsim_) {
	this->endOfFiles = blkdevsim->DEVICE_SIZE;
	struct myfs_header header;
	blkdevsim->read(0, sizeof(header), (char *)&header);
	endOfHeaders += sizeof(header);
	if (strncmp(header.magic, MYFS_MAGIC, sizeof(header.magic)) != 0 ||
	    (header.version != CURR_VERSION)) {
		std::cout << "Did not find myfs instance on blkdev" << std::endl;
		std::cout << "Creating..." << std::endl;
		format();
		std::cout << "Finished!" << std::endl;
	}
	else
	{
		blkdevsim->read(sizeof(header)  , sizeof(int ), (char*)&endOfTable);
		blkdevsim->read(sizeof(header) + sizeof(int)  , sizeof(int ), (char *)&endOfFiles);
		fileCount = (endOfTable - endOfHeaders) / sizeof (dir_list_entry);
	}
}

void MyFs::format() {
	struct myfs_header header;
	strncpy(header.magic, MYFS_MAGIC, sizeof(header.magic));
	header.version = CURR_VERSION;
	blkdevsim->write(0, sizeof(header), (const char*)&header);
	endOfTable = endOfHeaders;
	blkdevsim->write(sizeof(header)  , sizeof(int), (char*)&endOfTable);	
	blkdevsim->write(sizeof(header)  + sizeof(int)  , sizeof(int ), (char *)&endOfFiles);
	fileCount = 0;
}

void MyFs::create_file(std::string path_str, bool directory) {
	if (directory)
	{
		throw std::runtime_error("not implemented");
	}
	struct dir_list_entry dir;
	for (int i = endOfHeaders +1 ; i <=endOfTable ; i +=sizeof(dir_list_entry))
	{
		blkdevsim->read(i  , sizeof(dir_list_entry), (char*)&dir);
		std::string name = dir.name;
		if(name == path_str)
		{
			throw std::runtime_error("A file with that name already exists.");
		}
	}
	struct dir_list_entry newFile;
	char * cstr = new char [10];
  	strncpy (cstr, path_str.c_str(), 10);
	memcpy(newFile.name, cstr, 10);
	delete cstr;
	newFile.is_dir = directory;
	newFile.file_size = 0;
	newFile.address_start = endOfFiles;
	blkdevsim->write(endOfTable+1, sizeof(newFile), (char*)&newFile);
	endOfTable+=sizeof(newFile);
	blkdevsim->write(sizeof(myfs_header)  , sizeof(int), (char*)&endOfTable);
	fileCount = (endOfTable - endOfHeaders) / sizeof (dir_list_entry);
}

std::string MyFs::get_content(std::string path_str) {
	struct dir_list_entry dir;
	std::string res ;
	for (int i = endOfHeaders +1 ; i <=endOfTable ; i +=sizeof(dir_list_entry))
	{
		blkdevsim->read(i  , sizeof(dir_list_entry), (char*)&dir);
		std::string name = dir.name;
		if(name == path_str)
		{
			int address = dir.address_start;
			int size = dir.file_size;
			if (size == 0)
			{
				return "" ;
			}
			char  * data = new char [size];
			blkdevsim->read(address  , sizeof(char)* size, data);
			res = data;
			delete data;
			return res;
		} 
	}
	throw std::runtime_error("not Found!");
}

void MyFs::set_content(std::string path_str, std::string content) {	 
	//i decided to not check if the file was edited in terms of aquired new info because that would require comparing all of the memory and that would be as inefficient as recopying it
	//every time a file would be edited it would get re-written completly, but it would try to use any free space between existing blocks.
	struct dir_list_entry dir;
	bool found = false;
	for (int i = endOfHeaders +1 ; i <=endOfTable ; i +=sizeof(dir_list_entry))
	{
		blkdevsim->read(i  , sizeof(dir_list_entry), (char*)&dir);
		std::string name = dir.name;
		if(name == path_str)
		{
			dir.file_size = (0);
			blkdevsim->write(i  , sizeof(dir_list_entry), (char*)&dir); //update file entry 
			//this is done for the bestFitAlgorithem, this way it would check this memory too
			int addr = bestFit(content.length() + 2);
			addr -=(content.length() + 1);

			char * cstr = new char [content.length() + 1];
			strncpy (cstr, content.c_str(), content.length());
			cstr[content.length()] = 0; //remove newline charecter
			//endOfFiles -= (content.length() + 2) ; //update endoffiles
			blkdevsim->write(addr , sizeof(char) * (content.length() + 1) , cstr); // write data to addr
			//blkdevsim->write(sizeof(myfs_header)  + sizeof(int)  , sizeof(int ), (char *)&endOfFiles); //update enfOfFile veriable on start of file
			dir.file_size = (content.length() + 1); //update file size
			dir.address_start = (addr); //update file size
			blkdevsim->write(i  , sizeof(dir_list_entry), (char*)&dir); //update file entry
			updateEndOfFiles(); //update addressStart on all files sized zero
			delete cstr;
			found = true;
			break; // stop looking for entry
		} 
	}
	if (!found)
	{
		throw std::runtime_error("not Found!");
	}
}

MyFs::dir_list MyFs::list_dir(std::string path_str) {
	dir_list ans;
	struct dir_list_entry dir;

	for (int i = endOfHeaders +1 ; i <=endOfTable ; i +=sizeof(dir_list_entry))
	{
		blkdevsim->read(i  , sizeof(dir_list_entry), (char*)&dir);
		ans.push_back(dir);
	}
	return ans;
}

void MyFs::updateEndOfFiles()
{
	struct dir_list_entry dir;
	for (int i = endOfHeaders + 1 ; i <=endOfTable ; i +=sizeof(dir_list_entry))
	{
		blkdevsim->read(i  , sizeof(dir_list_entry), (char*)&dir);
		if (dir.file_size == 0)
		{
			dir.address_start = endOfFiles;
			blkdevsim->write(i  , sizeof(dir_list_entry), (char*)&dir);
		}
	}
} 

void print_bytes(const void *object, size_t size)
{
  const unsigned char * const bytes = static_cast<const unsigned char *>(object);
  size_t i;
  printf("[ ");
  for(i = 0; i < size; i++)
  {
    printf("%02x ", bytes[i]);
  }
  printf("]\n");
}


bool compareByStartAddress(const struct MyFs::dir_list_entry &a, const struct MyFs::dir_list_entry &b)
{
    return a.address_start < b.address_start;
}

int MyFs::bestFit(int size)
{
	dir_list allFiles= list_dir("");
	dir_list allocatedFiles ;
	dir_list emptyBlocks;
	for (std::vector<struct dir_list_entry>::iterator it = allFiles.begin(); it != allFiles.end(); ++it)
	{
		if (it->file_size != 0 && it->file_size  != 1 )
		{
			allocatedFiles.push_back(*it);

		}
	}
	sort(allocatedFiles.begin(), allocatedFiles.end(), compareByStartAddress);
	struct dir_list_entry cur;
	if (allocatedFiles.size() == 0)
	{
		cur.file_size = blkdevsim->DEVICE_SIZE - endOfTable;
		cur.address_start = blkdevsim->DEVICE_SIZE;
		emptyBlocks.push_back(cur);
	}
	else
	{
		cur.address_start = allocatedFiles[allocatedFiles.size() - 1]. address_start +allocatedFiles[allocatedFiles.size() - 1].file_size ;
		cur.file_size = blkdevsim->DEVICE_SIZE - cur.address_start  ; 
		emptyBlocks.push_back(cur);

		cur.address_start =allocatedFiles[0].address_start; 
		cur.file_size = allocatedFiles[0].address_start - endOfTable; 
		emptyBlocks.push_back(cur);

		int alocSize = allocatedFiles.size();

		for(int i =0 ; i < alocSize -1 ; i ++) 
		{
			cur.address_start = allocatedFiles[i + 1].address_start;
			cur.file_size =allocatedFiles[i + 1].address_start - (allocatedFiles[i].address_start + allocatedFiles[i].file_size);
			emptyBlocks.push_back(cur);
		}
	}
	int min = blkdevsim->DEVICE_SIZE;
	int index = -1;
	int vecSize = emptyBlocks.size();
	for(int i = 0 ; i < vecSize ; i ++) 
	{
		if (emptyBlocks[i].file_size < min && emptyBlocks[i].file_size >size )
		{
			min =emptyBlocks[i].file_size;
			index =i;
		} 
	}
	return index == -1  ? -1 :  emptyBlocks[index].address_start ;
}
